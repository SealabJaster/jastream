module jaster.stream.binary;

private
{
    import std.exception : enforce;
    import std.traits    : isNumeric, isSomeString, isDynamicArray;
    import std.range     : ElementType;
    import std.bitmanip  : bigEndianToNative, nativeToBigEndian, littleEndianToNative, nativeToLittleEndian;
    import std.utf       : validate;
    import std.format    : format;
    import std.typecons  : Nullable;
    import jaster.stream.core;
}

public import std.system : Endian, systemEndian = endian;

/++
 + A class used to easily write and read bytes to and from a `Stream`.
 +
 + This is useful for encoding data into a binary format.
 +
 + Notes:
 +  Functions are marked as either [Easy] or [Advanced].
 +
 +  [Easy] functions are designed to be easy to use, and should be fine
 +  for general cases.
 +
 +  [Advanced] functions are a bit less "automatic" or perform some other function,
 +  and are best used when the binary data needs to be a bit more customised.
 +
 + Throws:
 +  All read functions except `readBytes` will throw a `StreamException` if they suddenly reach the end of the stream when reading.
 +
 +  All write functions will throw a `StreamException` if they couldn't write out all of their bytes.
 + ++/
final class BinaryIO
{
    private
    {
        Stream _stream;
        Endian _endian;
    }

    /++
     + Assertions:
     +  `stream` must not be `null`.
     + ++/    
    this(Stream stream, Endian endian = systemEndian)
    {
        assert(stream !is null);
        this._stream        = stream;
        this._endian = endian;
    }

    // ###########
    // # READING #
    // ###########
    public
    {
        /++
         + [Advanced] Reads a certain amount of bytes.
         +
         + Notes:
         +  This is a slice into the underlying data instead of a copy.
         + ++/
        ubyte[] readBytes(size_t amount)
        {
            if(amount == 0)
                return null;

            return this._stream.read(amount);
        }

        /++
         + [Advanced] Reads in a length of something in a compact format.
         + ++/
        size_t readLengthBytes()
        {
            ubyte[4] data;
            data[0]   = this.read!ubyte();
            auto info = (this.endian == Endian.bigEndian)
                        ? data[0] & 0b1100_0000
                        : (data[0] & 0b0000_0011) << 6;

            T doConvert(T)(ubyte[T.sizeof] data)
            {
                auto v = (this.endian == Endian.bigEndian)
                         ? bigEndianToNative!T(data)
                         : littleEndianToNative!T(data);

                return (this.endian == Endian.bigEndian) 
                       ? v & ~(0b11 << ((T.sizeof * 8) - 2)) 
                       : v >> 2;
            }

            if(info == 0)
                return doConvert!ubyte(data[0..1]);
            else if(info == 0b0100_0000)
            {
                data[1] = this.read!ubyte();
                return doConvert!ushort(data[0..2]);
            }
            else if(info == 0b1000_0000)
            {
                data[1..$] = this.readBytes(3);
                return doConvert!uint(data[0..4]);
            }
            else
                throw new Exception("Length size info 0b1100_0000 is not used right now.");
        }

        /++
         + [Easy] Reads in a single numeric value.
         + ++/
        T read(T)()
        if(isNumeric!T)
        {
            auto bytes = this.readBytes(T.sizeof);
            enforce!StreamException(bytes.length == T.sizeof, format("Expected %s bytes, but only got %s.", T.sizeof, bytes.length));
            return (this.endian == Endian.bigEndian) 
                   ? bigEndianToNative!T(cast(ubyte[T.sizeof])bytes[0..T.sizeof])
                   : littleEndianToNative!T(cast(ubyte[T.sizeof])bytes[0..T.sizeof]);
        }

        /++
         + [Easy] Reads in an array of numeric values.
         + ++/
        T read(T)()
        if(isNumeric!(ElementType!T) && isDynamicArray!T)
        {
            auto length = this.readLengthBytes();
            T arr;
            foreach(i; 0..length)
                arr ~= this.read!(ElementType!T)();

            return arr;
        }

        /++
         + [Easy] Reads in a string.
         +
         + Notes:
         +  If the character type isn't immutable, then a slice to the underlying data is returned instead of
         +  a copy.
         +
         +  If it is immutable, then a `.idup` of the slice is returned.
         + ++/
        T read(T)()
        if(isSomeString!T)
        {
            auto length = this.readLengthBytes();
            auto slice  = this.readBytes(length);
            T    data;

            static if(is(ElementType!T == immutable))
                data = cast(T)slice.idup;
            else
                data = cast(T)slice;

            validate(data);
            return data;
        }
    }

    // ###########
    // # WRITING #
    // ###########
    public
    {
        /++
         + [Advanced] Writes out a series of bytes into the stream.
         +
         + Notes:
         +  $(B All) other write functions are based off of this function.
         +
         +  This function will grow the stream's size if needed.
         +
         +  Use this function if the [Easy] functions don't fit your use case.
         + ++/
        void writeBytes(scope const ubyte[] data)
        {
            auto amount = this._stream.write(data).length;
            enforce!StreamException(amount == data.length, format("Expected to write %s bytes, but could only write %s bytes.", data.length, amount));
        }

        /++
         + [Advanced] Writes out a length in a compact format.
         +
         + Details:
         +  This function aims to minimize the amount of bytes needed to write out the length of an array.
         +
         +  If the length is <= to 63, then a single byte is used.
         +
         +  If the length is <= to 16,383, then two bytes are used.
         +
         +  If the length is <= to 1,073,741,823, then four bytes are used.
         +
         +  In some cases it may be better to set a strict byte limit for an array, so this function may not be useful.
         +
         + Notes:
         +  Use this function if you need to write out the length of an array (outside of the [Easy] functions).
         +
         +  The last two bits are reserved for size info, so the max value of `length` is (2^30)-1, or 1,073,741,823
         +
         +  In big endian, the last two bits are used, since they will appear first in the data output.
         +  In little endian, the number is shifted to the left by two, so the first two bits can be used, again,
         +  because the first two bits will be in the first byte of the output.
         +
         + Throws:
         +  `Exception` if `length` is greater than 0x3FFFFFFF.
         + ++/
        void writeLengthBytes(size_t length)
        {
            // Last two bits are reserved for size info.
            // 00 = Length is one byte.
            // 01 = Length is two bytes.
            // 10 = Length is four bytes.
            enforce(length <= 0b00111111_11111111_11111111_11111111, "Length is too much");
            auto toWrite = (this.endian == Endian.bigEndian) ? length : length << 2;

            if(length <= 0b00111111) // Single byte
                this.write!ubyte(cast(ubyte)toWrite);
            else if(length <= 0b00111111_11111111) // Two bytes
            {
                toWrite |= (this.endian == Endian.bigEndian)
                           ? 0b01000000_00000000
                           : 0b00000000_00000001;
                this.write!ushort(cast(ushort)toWrite);
            }
            else // Four bytes
            {
                toWrite |= (this.endian == Endian.bigEndian)
                           ? 0b10000000_00000000_00000000_00000000
                           : 0b00000000_00000000_00000000_00000010;
                this.write!uint(cast(uint)toWrite);
            }
        }

        /++
         + [Easy] Writes a single numeric value.
         + ++/
        void write(T)(T value)
        if(isNumeric!T)
        {
            auto bytes = (this.endian == Endian.bigEndian) ? value.nativeToBigEndian
                                                           : value.nativeToLittleEndian;
            this.writeBytes(bytes[]);
        }

        /++
         + [Easy] Writes an array of numeric values.
         + ++/
        void write(T)(T[] value)
        if(isNumeric!T)
        {
            this.writeLengthBytes(value.length);
            foreach(val; value)
                this.write!T(val);
        }

        /++
         + [Easy] Writes a string.
         + ++/
        void write(T)(T[] value)
        if(is(T : const(char)) && !is(Unqual!T == ubyte))
        {
            auto bytes = cast(ubyte[])value;
            this.writeLengthBytes(bytes.length);
            this.writeBytes(bytes);
        }
    }

    // #########
    // # OTHER #
    // #########
    public
    {
        ///
        @property @safe @nogc
        inout(S) stream(S : Stream = Stream)() nothrow inout
        {
            return cast(inout(S))this._stream;
        }

        /// Sets the endianess of the numbers written/read by the high level functions.
        @property @safe @nogc
        inout(Endian) endian() nothrow inout
        {
            return this._endian;
        }

        ///
        @property @safe @nogc
        void endian(Endian e) nothrow
        {
            this._endian = e;
        }
    }
}
///
unittest
{
    import std.algorithm : reverse;
    import jaster.stream.memory;

    void doTest(Endian endian)
    {
        auto stream = new BinaryIO(new MemoryStreamGC());
        stream.endian = endian;

        // # Test numeric writes #
        stream.write!short(cast(short)0xFEED);
        stream.write!int(0xDEADBEEF);
        assert(stream.stream.length == 6);

        auto expected1 = (endian == Endian.bigEndian)
                         ? [0xFE, 0xED, 0xDE, 0xAD, 0xBE, 0xEF]
                         : [0xED, 0xFE, 0xEF, 0xBE, 0xAD, 0xDE];

        stream.stream.position = 0;
        assert(stream.readBytes(6) == expected1);

        stream.stream.position = 0;
        assert(stream.read!short == cast(short)0xFEED);
        assert(stream.read!int   == 0xDEADBEEF);

        // # Test length byte writes #
        stream = new BinaryIO(new MemoryStreamGC());
        stream.endian = endian;

        stream.writeLengthBytes(60);       // One byte
        stream.writeLengthBytes(16_000);   // Two bytes
        stream.writeLengthBytes(17_000);   // Four bytes

        auto expected2 = 
        (endian == Endian.bigEndian)
        ? [0b0011_1100,
           0b0111_1110, 0b1000_0000,
           0b1000_0000, 0b0000_0000, 0b0100_0010, 0b0110_1000]
        : [0b1111_0000,
           0b0000_0001, 0b1111_1010,
           0b1010_0010, 0b0000_1001, 0b0000_0001, 0b0000_0000];

        stream.stream.position = 0;
        assert(stream.readBytes(7) == expected2);
                                    
        stream.stream.position = 0;
        assert(stream.readLengthBytes() == 60);
        assert(stream.readLengthBytes() == 16_000);
        assert(stream.readLengthBytes() == 17_000);

        // # Test array writes #
        stream = new BinaryIO(new MemoryStreamGC());
        stream.endian = endian;

        stream.write!ushort([0xAABB, 0xCCDD, 0xEEFF]);

        auto expected3 = 
        (endian == Endian.bigEndian)
        ? [0x03, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF]
        : [0x0C, 0xBB, 0xAA, 0xDD, 0xCC, 0xFF, 0xEE];

        stream.stream.position = 0;
        assert(stream.readBytes(7) == expected3);

        stream.stream.position = 0;
        assert(stream.read!(ushort[]) == [0xAABB, 0xCCDD, 0xEEFF]);

        // # Test string writes #
        stream = new BinaryIO(new MemoryStreamGC());
        stream.endian = endian;

        stream.write("Gurl");

        auto expected4 = (endian == Endian.bigEndian) ? 0x04 : 0x04 << 2;
        stream.stream.position = 0;
        assert(stream.readBytes(stream.stream.length) == [expected4, 'G', 'u', 'r', 'l']);

        stream.stream.position = 0;
        assert(stream.read!string() == "Gurl");
    }

    doTest(Endian.bigEndian);
    doTest(Endian.littleEndian);
}