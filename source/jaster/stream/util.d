module jaster.stream.util;

private
{
    import std.traits : isPointer;
    import std.experimental.allocator;
}

struct AllocHelper(Alloc)
{
    static if(stateSize!Alloc == 0 && !isPointer!Alloc)
    {
        import std.traits : hasMember;
        static assert(hasMember!(Alloc, "instance"), "Static allocators must have a member named 'instance'");
        
        public alias alloc    = Alloc.instance;
        public enum  IsStatic = true;
    }
    else
    {
        public Alloc alloc;
        public enum  IsStatic = false;
    }

    alias alloc this;
}