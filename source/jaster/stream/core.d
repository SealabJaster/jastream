module jaster.stream.core;

private
{
    import std.experimental.allocator : makeArray, dispose;
    import std.exception              : basicExceptionCtors, enforce;
    import jaster.stream.util;
}

public import core.time : Duration;

/++
 + The base class for all streams.
 +
 + Streams are a common interface for reading/writing a 'stream' of bytes.
 +
 + Other classes can be built on top of streams, such as the `BinaryIO` class to provide more
 + specific functionality to streams.
 + ++/
abstract class Stream
{
    /// A flag enum specifying what the stream is capable of doing.
    enum Can
    {
        None = 0,

        /// The stream supports writing.
        Write   = 1 << 0,

        /// The stream supports reading.
        Read    = 1 << 1,

        /// The stream supports seeking.
        Seek    = 1 << 2,

        /// The stream supports being able to set timeouts.
        Timeout = 1 << 3
    }

    /// Used with `seek`.
    enum SeekFrom
    {
        /// Seek from the start.
        Start,

        /// Seek from the current position.
        Current,

        /++
         + Seek from the end.
         +
         + Implementation_Note:
         +  Some might think that when using `End`, that the amount to seek by means 'How many spaces to go backwards'.
         +
         +  This however, is not how I feel it should work. So if you want to go backwards from the end, just use a negative offset,
         +  like with the other options.
         + ++/
        End
    }

    private
    {
        Can _can;
    }

    ///
    @safe @nogc
    this(Can can) nothrow pure
    {
        this._can = can;
    }

    // ######################
    // # ABSTRACT FUNCTIONS #
    // ######################
    public abstract
    {
        /++
         + Writes bytes into the stream.
         +
         + Params:
         +  data = The data to write.
         +
         + Throws:
         +  `StreamCannotWriteException` if this stream does not support writing.
         +  `StreamDisposedException` if this stream has been disposed of.
         +
         + Returns:
         +  A slice of `data`, specifying which portion of it has been written to the stream.
         + ++/
        const(ubyte[]) write(scope const ubyte[] data);

        /++
         + Reads bytes from the stream into a buffer.
         +
         + Params:
         +  buffer = The buffer to read into, the length of the buffer determines how many bytes to read.
         +
         + Throws:
         +  `StreamCannotReadException` if this stream does not support reading.
         +  `StreamDisposedException` if this stream has been disposed of.
         +
         + Returns:
         +  A slice of `buffer`, specifying which portion of it has been filled with data.
         +  The length of this slice is also how to check how many bytes were read in.
         + ++/
        ubyte[] readToBuffer(scope ref ubyte[] buffer);

        /++
         + Disposes of the stream's resources.
         +
         + If the stream has already been disposed of, this function does nothing.
         +
         + Once disposed of, the stream should cease to function, and should throw `StreamDisposedException`s where appropriate.
         + ++/
        void dispose();

        /++
         + Flushes the stream's data.
         +
         + Some streams don't support flushing, so this function will do nothing in those cases.
         + ++/
        void flush();

        /++
         + Seeks somewhere into the stream.
         +
         + Throws:
         +  `StreamCannotSeekException` if this stream does not support seeking.
         +  `StreamDisposedException` if this stream has been disposed of.
         +
         + Params:
         +  from   = The 'origin' from where to seek from.
         +  amount = The amount to seek by. Note that negative numbers can be used to go backwards.
         + ++/
        void seek(SeekFrom from, ptrdiff_t amount);

        /// Returns: If the stream has been disposed of or not.
        @property
        bool isDisposed() const;

        /++
         + Sets the write timeout for the stream.
         +
         + Throws:
         +  `StreamCannotTimeoutException` if the stream does not support timeouts.
         +  `StreamDisposedException` if the stream has been disposed of.
         +
         + Params:
         +  dur = The duration to set the timeout for.
         + ++/
        @property
        void writeTimeout(Duration dur);

        /++
         + Gets the write timeout for the stream.
         +
         + Throws:
         +  `StreamCannotTimeoutException` if the stream does not support timeouts.
         +  `StreamDisposedException` if the stream has been disposed of.
         +
         + Returns:
         +  The duration to set the timeout for.
         + ++/
        @property
        Duration writeTimeout();

        /++
         + Sets the read timeout for the stream.
         +
         + Throws:
         +  `StreamCannotTimeoutException` if the stream does not support timeouts.
         +  `StreamDisposedException` if the stream has been disposed of.
         +
         + Params:
         +  dur = The duration to set the timeout for.
         + ++/
        @property
        void readTimeout(Duration dur);

        /++
         + Gets the read timeout for the stream.
         +
         + Throws:
         +  `StreamCannotTimeoutException` if the stream does not support timeouts.
         +  `StreamDisposedException` if the stream has been disposed of.
         +
         + Returns:
         +  The duration to set the timeout for.
         + ++/
        @property
        Duration readTimeout();

        /++
         + Sets the length of the stream's data.
         +
         + Throws:
         +  `StreamException` if the stream does not support this function.
         +  `StreamDisposedException` if the stream has been disposed of.
         +
         + Params:
         +  size = The size to set the stream's length to.
         + ++/
        @property
        void length(size_t size);

        /++
         + Gets the length of the stream's data.
         +
         + Throws:
         +  `StreamException` if the stream does not support this function.
         +  `StreamDisposedException` if the stream has been disposed of.
         +
         + Returns:
         +  The size to set the stream's length.
         + ++/
        @property
        size_t length();

        /++
         + Sets the position of the stream's 'cursor'.
         +
         + Throws:
         +  See `seek`.
         +
         + Params:
         +  pos = The position to use.
         + ++/
        @property
        void position(size_t pos);

        /++
         + Gets the position of the stream's 'cursor'.
         +
         + Throws:
         +  See `seek`.
         +
         + Returns:
         +  The position to use.
         + ++/
        @property
        size_t position();
    }

    // #####################
    // # VIRTUAL FUNCTIONS #
    // #####################
    public
    {
        /++
         + Creates a GC-allocated buffer with a specified size, and attempts to read in a certain amount of bytes.
         +
         + Params:
         +  amount = The amount of bytes to read in.
         +
         + Returns:
         +  The portion of the buffer that was filled by `readToBuffer`.
         + ++/
        ubyte[] read(size_t amount)
        {
            auto array = new ubyte[amount];
            return this.readToBuffer(array);
        }

        /++
         + Copies data $(B from the current position in this stream) to the end of this stream, into
         + the given stream. $(B The given stream is written to using it's current position as well).
         +
         + Params:
         +  to      = The `Stream` to copy the data to.
         +  buffer  = The buffer used to temporarily store the data as it's being copied over.
         +            This buffer also determines how many bytes are copied at a time.
         + ++/
        void copyTo(Stream to, scope ubyte[] buffer)
        {
            // Slight optimisation. Only useful for when the buffer is a small size relative to how much is being copied.
            // It also depends on the stream itself whether it has any real impact or not.
            if(this.canSeek && to.canSeek)
            {
                auto end = to.position + (this.length - this.position);
                if(end > to.length)
                    to.length = end;
            }

            while(true)
            {
                auto slice = this.readToBuffer(buffer);
                if(slice.length == 0)
                    break;

                to.write(slice);
            }
        }
    }

    // ##############################
    // # FINAL FUNCTIONS/PROPERTIES #
    // ##############################
    public final
    {
        /// Shortcut for `seek(SeekFrom.Start)`
        void seekStart(ptrdiff_t amount)
        {
            this.seek(SeekFrom.Start, amount);
        }

        /// Shortcut for `seek(SeekFrom.Current)`
        void seekCurr(ptrdiff_t amount)
        {
            this.seek(SeekFrom.Current, amount);
        }

        /// Shortcut for `seek(SeekFrom.End)`
        void seekEnd(ptrdiff_t amount)
        {
            this.seek(SeekFrom.End, amount);
        }

        /++
         + A helper function for `copyTo`, that uses a buffer placed on the stack.
         +
         + Params:
         +  BufferSize = The size to give the buffer.
         +  to         = The stream to copy the data to.
         + ++/
        void copyToStack(size_t BufferSize)(Stream to)
        if(BufferSize > 0)
        {
            ubyte[BufferSize] buffer;
            this.copyTo(to, buffer[]);
        }

        /++
         + A helper function for `copyTo`, that uses a buffer created from an `std.experimental.allocator` Allocator.
         + The buffer is of course disposed of afterwards.
         +
         + Params:
         +  Alloc       = The allocator to use.
         +  to          = The stream to copy the data to.
         +  bufferSize  = The size to give the buffer.
         +  alloc       = [Only for non-static allocator] the allocator instance to use.
         + ++/
        void copyToAlloc(Alloc)(Stream to, size_t bufferSize)
        if(AllocHelper!Alloc.IsStatic)
        {
            assert(bufferSize > 0, "Buffer size cannot be 0");
            ubyte[] buffer = Alloc.instance.makeArray!ubyte(bufferSize);
            scope(exit) Alloc.instance.dispose(buffer);

            this.copyTo(to, buffer);
        }

        /// ditto.
        void copyToAlloc(Alloc)(Stream to, size_t bufferSize, auto ref Alloc alloc)
        if(!AllocHelper!Alloc.IsStatic)
        {
            assert(bufferSize > 0, "Buffer size cannot be 0");
            ubyte[] buffer = alloc.makeArray!ubyte(bufferSize);
            scope(exit) alloc.dispose(buffer);

            this.copyTo(to, buffer);
        }

        /++
         + A helper function for `copyTo`, that uses a buffer allocated by the GC.
         +
         + Params:
         +  to         = The stream to copy the data to.
         +  bufferSize = The size to give the buffer.
         + ++/
        void copyToGC(Stream to, size_t bufferSize)
        {
            assert(bufferSize > 0, "Buffer size cannot be 0");
            ubyte[] buffer = new ubyte[bufferSize];

            this.copyTo(to, buffer);
        }

        /// Returns: What the stream is capable of.
        @property @safe @nogc
        Can can() nothrow const
        {
            return this._can;
        }

        /// Returns: Whether the stream can write.
        @property @safe @nogc
        bool canWrite() nothrow const
        {
            return (this.can & Can.Write) > 0;
        }

        /// Returns: Whether the stream can Read.
        @property @safe @nogc
        bool canRead() nothrow const
        {
            return (this.can & Can.Read) > 0;
        }

        /// Returns: Whether the stream can Seek.
        @property @safe @nogc
        bool canSeek() nothrow const
        {
            return (this.can & Can.Seek) > 0;
        }

        /// Returns: Whether the stream can Timeout.
        @property @safe @nogc
        bool canTimeout() nothrow const
        {
            return (this.can & Can.Timeout) > 0;
        }
    }
}

/++
 + Throws an exception if the given stream does not support writing/reading/seeking/timeouts.
 + ++/
void enforceCanWrite(const Stream stream)
{
    enforce!StreamCannotWriteException(stream.canWrite);
}

/// ditto
void enforceCanRead(const Stream stream)
{
    enforce!StreamCannotReadException(stream.canRead);
}

/// ditto
void enforceCanSeek(const Stream stream)
{
    enforce!StreamCannotSeekException(stream.canSeek);
}

/// ditto
void enforceCanTimeout(const Stream stream)
{
    enforce!StreamCannotTimeoutException(stream.canTimeout);
}

/// ditto
void enforceNotDisposed(const Stream stream)
{
    enforce!StreamDisposedException(!stream.isDisposed);
}

/// Base stream exception class.
class StreamException : Exception
{
    mixin basicExceptionCtors;
}

/// Thrown if the stream doesn't support writing.
class StreamCannotWriteException : StreamException
{
    mixin basicExceptionCtors;

    this(string file = __FILE__, size_t line = __LINE__, Throwable next = null)
    {
        super("This stream does not support writing/is read only.", file, line, next);
    }
}

/// ditto
class StreamCannotReadException : StreamException
{
    mixin basicExceptionCtors;

    this(string file = __FILE__, size_t line = __LINE__, Throwable next = null)
    {
        super("This stream does not support reading.", file, line, next);
    }
}

/// ditto
class StreamCannotSeekException : StreamException
{
    mixin basicExceptionCtors;

    this(string file = __FILE__, size_t line = __LINE__, Throwable next = null)
    {
        super("This stream does not support seeking.", file, line, next);
    }
}

/// ditto
class StreamCannotTimeoutException : StreamException
{
    mixin basicExceptionCtors;

    this(string file = __FILE__, size_t line = __LINE__, Throwable next = null)
    {
        super("This stream does not support timing out.", file, line, next);
    }
}

/// ditto
class StreamDisposedException : StreamException
{
    mixin basicExceptionCtors;

    this(string file = __FILE__, size_t line = __LINE__, Throwable next = null)
    {
        super("This stream has been disposed.", file, line, next);
    }
}