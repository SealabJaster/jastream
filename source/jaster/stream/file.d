module jaster.stream.file;

private
{
    import std.stdio     : File, SEEK_CUR, SEEK_END, SEEK_SET;
    import std.file      : exists, fwrite = write;
    import std.exception : enforce;
    import std.typecons  : Flag;
    import jaster.stream;
}

/++
 + This is a `Stream` that writes/reads directly from, and to a file.
 +
 + It uses `std.stdio.File` internally.
 +
 + Other than exceptions, this class does not allocate any GC memory (though the same cannot be said for `File`,
 + I haven't checked yet).
 + ++/ 
class FileStream : Stream
{
    alias DisposeOnDtor = Flag!"doDtor";

    /// Describes how the file should be opened.
    ///
    /// This will also determine the capabilities (`Stream.Can`) of the stream.
    enum Mode : int
    {
        /// Opens the $(B only) if it exists.
        Open     = 1 << 0,

        /// Creates the file if it doesn't exist, and then acts the same as `Mode.Open`.
        Create   = 1 << 1,

        /// Supports reading.
        Read     = 1 << 2,
        
        /// Supports writing.
        Write    = 1 << 3,

        /// Opens the file in binary mode.
        Binary   = 1 << 4,

        /// Sets the file's size to 0 upon opening. $(B Does not imply `Mode.Create`)
        ///
        /// Combine with Mode.Create to always make sure to either create, or truncate a file, meaning it's always there
        /// and always empty.
        Truncate = 1 << 5,

        ReadWrite             = Read         | Write,
        OpenOrCreate          = Open         | Create,
        ReadOpen              = Open         | Read,
        ReadCreate            = Create       | Read,
        ReadOpenOrCreate      = OpenOrCreate | Read,
        WriteOpen             = Open         | Write,
        WriteCreate           = Create       | Write,
        WriteOpenOrCreate     = OpenOrCreate | Write,
        ReadWriteOpen         = Open         | Read  | Write,
        ReadWriteCreate       = Create       | Read  | Write,
        ReadWriteOpenOrCreate = OpenOrCreate | Read  | Write
    }

    private
    {
        File            _file;
        Mode            _mode;
        DisposeOnDtor   _doDtor;

        static const ubyte[] EMPTY_DATA = [];
    }

    /++
     + Opens the specified file.
     +
     + Notes:
     +  During destruction of a stream made from this constructor, the `dispose` function will
     +  be called if the object is not already disposed.
     +
     + Throws:
     +  `StreamException` if `file` could not be found.
     +
     +  `StreamException` if `mode` doesn't specify either `Open`, `Create` in some form.
     +
     + Params:
     +  mode = The `FileStream.Mode` to open the `file` in.
     +  file = The file to open.
     + ++/
    this(Mode mode, const char[] file)
    {
        if((mode & Mode.Create) && !file.exists)
            fwrite(file, EMPTY_DATA);

        if(mode & Mode.Open)
            enforce!StreamException(file.exists, "File not found: "~file);
        else if(!(mode & Mode.Create))
            throw new StreamException("The given mode does not specify Create, Open, or OpenOrCreate.");

        char[3] buffer;
        this._file   = File(file, mode.modeToString(buffer));
        this._mode   = mode;
        this._doDtor = DisposeOnDtor.yes;

        super(mode.canFromMode());        
    }

    /++
     + Wraps around an existing `File`.
     +
     + Assertions:
     +  `file` must have a file already opened.
     +
     + Notes:
     +  Unlike the other constructor, objects made from this constructor will only call `dispose`
     +  during the dtor if `doDtor` is set to `DisposeOnDtor.yes`.
     +
     +  This means, that if either this stream is disposed manually, or this object is collected/destroyed,
     +  then external copies of the `file` will also end up being closed. Which could possibly lead to confusion.
     +
     +  Also note that due to how this stream's `isDispose` works, if `file` is closed outside of this stream, then
     +  this stream will begin to throw `StreamDisposedException`s.
     +
     + Params:
     +  mode   = The `FileStream.Mode` to use. For this constructor, `Open`, `Create`, and `Truncate` have no effect.
     +  file   = The `File` to wrap around.
     +  doDtor = See the `Notes` section.
     + ++/
    this(Mode mode, File file, DisposeOnDtor doDtor = DisposeOnDtor.yes)
    {
        assert(file.isOpen, "Make sure the file is actually open first.");

        this._file   = file;
        this._mode   = mode;
        this._doDtor = doDtor;
        
        super(mode.canFromMode());
    }

    ~this()
    {
        if(this._doDtor)
            this.dispose();
    }

    public override
    {	
        const(ubyte[]) write(scope const ubyte[] data) 
        {
            enforceNotDisposed(this);
            enforceCanWrite(this);
            this._file.rawWrite(data);

            return data; // rawWrite throws an exception if the entire thing isn't written... Maybe I should wrap it around a StreamException
        }
        
        ubyte[] readToBuffer(scope ref ubyte[] buffer) 
        {
            enforceNotDisposed(this);
            enforceCanRead(this);

            return this._file.rawRead(buffer);
        }
        
        void dispose() 
        {
            if(!this.isDisposed)
                this.flush();

            this._file.close();
        }
        
        void flush() 
        {
            enforceNotDisposed(this);
            this._file.flush();
            this._file.sync();
        }
        
        void seek(SeekFrom from, ptrdiff_t amount) 
        {
            enforceNotDisposed(this);
            auto start = (from == SeekFrom.Start)
                         ? SEEK_SET
                         : (from == SeekFrom.Current)
                            ? SEEK_CUR
                            : SEEK_END;
            
            auto oldPos = this.position;
            this._file.seek(cast(long)amount, start);

            if(this.position > this.length)
            {
                import std.format : format;
                auto debugPos = this.position;
                this.position = oldPos;
                throw new StreamException(
                    format("Attempted to seek past the stream. Destination = %s. Length = %s",
                        debugPos,
                        this.length
                    )
                );
            }
        }
        
        /// For FileStreams, this function will return `true` if the underlying `File` has been closed.
        @property
        bool isDisposed() const
        {
            return !this._file.isOpen;
        }

        @property
        void writeTimeout(Duration dur)
        {
            enforceNotDisposed(this);
            throw new StreamCannotTimeoutException();
        }

        @property
        Duration writeTimeout()
        {
            enforceNotDisposed(this);
            throw new StreamCannotTimeoutException();
        }

        @property
        void readTimeout(Duration dur)
        {
            enforceNotDisposed(this);
            throw new StreamCannotTimeoutException();
        }

        @property
        Duration readTimeout()
        {
            enforceNotDisposed(this);
            throw new StreamCannotTimeoutException();
        }
        
        @property 
        void length(size_t size) 
        {
            if(size > this.length)
            {
                auto amount = (size - this.length);
                auto pos    = this.position;
                scope(exit) this.position = pos;

                this._file.seek(size - 1, SEEK_SET);

                byte b = 0;
                this._file.rawWrite((&b)[0..1]);
            }
        }
        
        @property 
        size_t length() 
        {
            enforceNotDisposed(this);

            auto size = this._file.size();
            enforce!StreamException(size <= size_t.max, "The size is out of bounds of size_t.max");
            return cast(size_t)size;
        }
        
        @property 
        void position(size_t pos) 
        {
            this.seek(SeekFrom.Start, pos);
        }
        
        @property 
        size_t position()
        {
            enforceNotDisposed(this);

            auto pos = this._file.tell();
            enforce!StreamException(pos <= size_t.max, "The position is out of bounds of size_t.max");
            return cast(size_t)pos;
        }
    }    
}
///
unittest
{
    import std.exception : assertThrown;

    auto mode   = FileStream.Mode.ReadWriteCreate | FileStream.Mode.Truncate | FileStream.Mode.Binary;
    auto stream = new FileStream(mode, "tests/fs2.bin");

    stream.length = 200;
    assert(stream.length == 200);

    assertThrown!StreamException(stream.position = 201);
}

// Stream.Can checks
unittest
{
    import std.exception : assertThrown;

    assert( new FileStream(FileStream.Mode.WriteCreate, "tests/fs1.bin").canWrite);
    assert( new FileStream(FileStream.Mode.ReadCreate,  "tests/fs1.bin").canRead);
    assert(!new FileStream(FileStream.Mode.WriteCreate, "tests/fs1.bin").canRead);

    assertThrown!StreamCannotWriteException(new FileStream(FileStream.Mode.ReadOpen, "tests/fs1.bin").write([1]));
    assertThrown!StreamCannotReadException(new FileStream(FileStream.Mode.WriteOpen, "tests/fs1.bin").read(1));
}

// Dispose checks
unittest
{
    import std.exception : assertThrown;

    auto stream = new FileStream(FileStream.Mode.WriteCreate, "tests/fs1.bin");
    assert(!stream.isDisposed);
    stream.dispose();
    assert(stream.isDisposed);

    assertThrown!StreamDisposedException(stream.write([1]));
}

/// Returns: A `Stream.Can` from a `FileStream.Mode`.
Stream.Can canFromMode(FileStream.Mode mode)
{
    Stream.Can can = Stream.Can.None;

         if(mode & FileStream.Mode.Write) can |= Stream.Can.Write;
    else if(mode & FileStream.Mode.Read)  can |= Stream.Can.Read;

    return can;
}

private char[] modeToString(FileStream.Mode mode, return out char[3] str)
{
    size_t plusIndex = 1;
    
    if(mode & FileStream.Mode.Binary)
    {
        str[1]    = 'b';
        plusIndex = 2;
    }

    if(mode & FileStream.Mode.ReadWrite)
        str[plusIndex] = '+';

    // Because C is awkward, we'll just make the file ourself if it doesn't exist.
    // Being able to open a file, with write-only access, *without* truncating the file, seems impossible.
    str[0] = (mode & FileStream.Mode.Truncate) ? 'w' : 'r';

    return str[0..plusIndex+1];
}