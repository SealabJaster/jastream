# About this library

This library intends to provide streams that are similar to the streams you find in .Net, simply because I miss them when coding in D.

I'd like to note that this library makes use of exceptions for most of it's error handling.

# MemoryStream

The memory stream, as the name implies, is a stream that writes directly into memory (as opposed to a file or socket).

This stream has support for `std.experimental.allocator`, so the programmer can decide how it allocates memory.

This stream doesn't use the GC except for exceptions, or if the given allocator uses it.

# FileStream

The file stream is self explanitory.

This stream doesn't use the GC except for exceptions, or if `std.stdio.File` uses it.

# BinaryIO

This is a helper class that can wrap around any `Stream` object to provide an easy way to turn primitive types and arrays of primitive types
into bytes.

The endianess of numbers is customisable.

This also means that this stream makes liberal use of the GC.

# jaster.stream.range

This module provides functions to make streams work as ranges, as well as specific range functions useful for streams.

## byChunkAlloc

This function will use a given `std.experimental.allocator` Allocator to allocate a buffer of a specified size.
The returned range uses a `RefCounted` internally for it's buffer, the type used with `RefCounted` will call the `dispose`
function on the buffer using the given allocator once it's ref-count reaches 0.

An input range is then returned which can use this buffer to provide chunks of a `Stream`'s data at a time.

If the buffer is larger than the amount of data read from the stream, then only the portion of the buffer that was filled with data is returned by the
`front` function.

## byChunkGC

This function functions the same as `byChunkAlloc`, except that it uses a GC allocated array as the buffer, and does not rely on
ref-counting.

# Plans

I intend to implement the following streams:

* MemoryMappedStream

* NetworkStream (might call it SocketStream)

At some point I will probably also write a helper class akin to BinaryIO, to work with the various strings D has for when writing text data.

I'm considering marking all of `Stream`'s functions as `@safe`, forcing implementors to make sure their code is also `@safe` or `@trusted`.
This has it's own set of issues though, so it'll require some thought before I come to a decision.

# Examples

There's no real examples at the moment, but both the [MemoryStream](https://gitlab.com/SealabJaster/jastream/blob/master/source/jaster/stream/memory.d#L184)
and [BinaryIO](https://gitlab.com/SealabJaster/jastream/blob/master/source/jaster/stream/binary.d#L249) classes have a unittest going over most of it's functionality.
Though, these are more for testing rather than a documented example of how to use the classes.